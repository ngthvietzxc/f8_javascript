/*
Hàm (function) trong javascript
1. Hàm?
- Một khối mã
- Làm 1 việc cụ thể
2. Loại hàm
- Built-in
- Tự định nghĩa
3. Tính chất
- Không thực thi khi định nghĩa
- Sẽ thực thi khi được gọi
- Có thể nhận tham số
- Có thể trả về 1 giá trị
4. Tạo hàm đầu tiên
*/

// Qui tắc đặt tên biến a-z A-Z 0-9 _ $
function showDialog() {
  alert("Hi xin chao cac ban");
}
showDialog();

// Tham số hàm
/*
1. Tham số?
- Định nghĩa?
- Kiểu dữ liệu?
- Tính Private?
2. Truyền tham số
- 1 tham số 
- Nhiều tham số
3. Argument?
- Đối tượng arguments
- Giới thiệu vòng for of
*/

// Argument
function writeLog() {
  var myString = "";
  for (var param of arguments) {
    myString += `${param} - `;
  }
  console.log(myString);
}
writeLog("hhh", "jjjj");

// Return trong hàm - Javascript cơ bản
function cong(a, b) {
  return { a, b };
}
var result = cong(2, 8);
console.log(result);

/*
Một số điều cần biết về function
    1. Khi function đặt trùng tên
    2. Khai báo biến trong hàm: khi khai bao bien trong function chi su dung trong function
    3. Định nghĩa trong hàm
*/

/*
Các loại function
  1. Declaration function
  2. Expression function
  3. Arrow Function
*/

// Declaration function
function showMessage() {}
// Expression function
var showMess = function () {};
