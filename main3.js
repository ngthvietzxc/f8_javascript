var fullName = "Nguyen Thanh Viet";
var age = "25";

alert(fullName);
alert(age);

/* 
Giới thiệu một số hàm built-in
    1.	Alert
    2.	Console
    3.	Confirm
    4.	Prompt
    5.	Set timeout
    6.	Set interval
*/

console.log(fullName);
confirm("Xac nhan ban du tuoi!");
prompt("Xac nhan ban du tuoi");
setTimeout(function () {
  alert("Thon gbao");
}, 10);
// setInterval(function () {
//   console.log("Thong bao");
// }, 10);

/*
Toán tử trong Javascript
    1.	Toán tử số học – Arithmetic
    2.	Toán tử gán – Assignment
    3.	Toán tử so sánh – Comparíson
    4.	Toán tử logic - Logical
*/
// Toan tu so hoc
var a = 1 + 2;
console.log(a);
// Toan tu gan
var fullName1 = "Thanh Viet Nguyen";
// Toan tu so sanh
var x = 1;
var y = 1;
if (x == y) {
  console.log("yes");
}

/*
Toán tử số học
    +       -->     Cộng
    -       -->     Trừ
    *       -->     Nhân
    **      -->     Luỹ thừa
    /       -->     Chia
    &       -->     Chia lấy dư
    ++      -->     Tăng 1 giá trị số
    --      -->     Giảm 1 giá trị số
*/

/*
Toán tử gán
    Toán tử         Ví dụ       Tuơng đươngg     
        =           x = y           x = y 
        +=          x += y          x = x + y
        -=          x -= y          x = x - y
        *=          x *= y          x = x * y
        /=          x /= y          x = x / y
        **=         x **= y         x = x ** y
*/

/*
Toán tử chuỗi - String operator
*/

var firstName = "Nguyen";
var lastName = " Viet";
console.log(firstName + "V" + lastName);

/*
Toán tử so sánh
    ==  Bằng
    !=  Không bằng  
    >   Lớn hơn
    <   Nhỏ hơn
    >=  Lớn hơn hoặc bằng
    <=  Nhỏ hơn hoặc bằng
*/

var d = 1;
var e = 2;
if (d > e) {
  console.log("Dung");
} else {
  console.log("Sai");
}

/*
Boolean
*/
var isSuccess = true;
console.log(isSuccess);

var age = 16;
var canBuyAlcohol = age > 18;
console.log(canBuyAlcohol);

/* 
Toán tử logical
    1. && - And
    2. || - Or
    3. !  - Not
*/
var b = 1;
var n = 2;
var m = 3;
if (b > 0 && n > 0) {
  console.log("Dieu kien dung");
} else {
  console.log("Dieu kien sai");
}

/*
Kiểu dữ liệu trong Javascript
    1. Dữ liệu nguyên thuỷ - Primitive Data
        - Number
        - String
        - Boolean
        - Undefined
        - Null
        - Symbol
    2. Dữ liệu phức tạp - Complex Data
        - Function
        - Object
*/

// Number
var u = 1;
// String type
var fuName = "Viet";
// Bôolean
var isSucc = true;
// Undifined type
var tuoi;
// Null
var isNull = null;
// Symbol
var id = Symbol("description");
// Function
var myFunction = function () {
  alert("Hi");
};
myFunction();
// Object types
var myObject = {
  name: "Viet",
  age: 18,
  myFunction: function () {},
};
// Array
var myArray = ["viet", "ruby", "viii"];

/*
Toán tử so sánh phần 2
    1. === (So sánh kiểu dữ liệu)
    2. !== (So sánh kiểu dữ liệu)
*/
var aa = 1;
var bb = 1;
console.log(aa === bb);

/* Truthy và Falsy
// Bất cứ giá trị nào trong Javascript khi chuyển đổi sang kiểu dữ liệu boolean mà có giá trị true thì ta gọi giá trị đó là Truthy
// Bất cứ giá trị nào trong Javascript khi chuyển đổi sang kiểu dữ liệu boolean mà có giá trị false thì ta gọi giá trị đó là Falsy.
*/
console.log(Boolean(1)); // true
console.log(Boolean(["BMW"])); // true
console.log(Boolean({ name: "Miu" })); // true
console.log(!!"hi"); // true
console.log(!!1); // true
console.log(!!"f8"); // true
console.log(!!["Mercedes"]); // true
console.log(!!false); // false
console.log(!!0); // false
console.log(!!""); // false
console.log(!!null); // false
console.log(!!undefined); // false
console.log(!!NaN); // false
